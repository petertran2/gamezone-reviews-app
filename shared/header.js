import React from 'react'
import { StyleSheet, Text, View, Image, ImageBackground } from 'react-native'
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons'

export default function Header({ navigation, title }) {

  const openMenu = () => {
    navigation.openDrawer()
  }

  return (
    <ImageBackground source={require('../assets/game_bg.png')} style={styles.header}>
      <MaterialIcon name='menu' size={28} onPress={openMenu} style={styles.icon} />
      <View style={styles.headerTitle}>
        <Image source={require('../assets/heart_logo.png')} style={styles.headerImage}/>
        <Text style={styles.headerText}>{ title }</Text>
      </View>
    </ImageBackground>
  )
}

const styles = StyleSheet.create({
  header: {
    backgroundColor: '#eee',
    padding: 15
  },
  headerTitle: {
    flexDirection: 'row',
    marginLeft: 'auto',
    marginRight: 'auto'
  },
  headerText: {
    fontWeight: 'bold',
    fontSize: 20,
    color: '#333',
    letterSpacing: 1
  },
  icon: {
    position: 'absolute',
    left: 16,
    top: 15
  },
  headerImage: {
    width: 26,
    height: 26,
    marginHorizontal: 10
  }
})
